<?php

function getFiles() {
    $files = array();
    if ($dir = opendir('.')) {
        while ($file = readdir($dir)) {
            if ($file != '.' && $file != '..' && $file != basename(__FILE__) && $file != "index.php" && $file != "nbproject") {
                $files[] = $file;
            }
        }
        closedir($dir);
    }
    natsort($files); //sort
    return $files;
}
?>

<html>
    <head>
    </head>
    <body>

        <h1> List of files </h1>

        <ul class="dir">
            <?php
            foreach (getFiles() as $file)
                echo "<li name='$file'><a href='$file'>$file</a></li>";
            ?>
        </ul>

    </body>
</html>