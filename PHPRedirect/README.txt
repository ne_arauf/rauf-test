WorldPay Hosted Payment Page (HTML Redirect) - Select Junior Integration
Copyright (C) 2013 WorldPay
Support: support@worldpay.com

------------------------------------------------------

Created:		18/02/2013
Created By:		Sam Robbins, WorldPay
Version:		1.0
Language: 		PHP

------------------------------------------------------

Terms of Use:

These terms are supplemental to your relevant Merchant Services Agreement with WorldPay and apply to your use of the attached software, code, scripts documentation and files (the "Code"). 

The Code may be modified without limitation by WorldPay.

The Code is provided solely for the purpose of integrating the Customer's system with the relevanht WorldPay Gateway and must not be used or modified in any way to allow it to work with any other gateway/payment system other than that which is provided by the WorldPay group of companies.

------------------------------------------------------

Disclaimer: 
The Code is provided 'as is' without warranty of any kind, either express or implied, including, but not limited to, the implied warranties of fitness for a purpose, or the warranty of non-infringement. Without limiting the foregoing,WorldPay makes no warranty that:
i.the Code will meet your requirements
ii.the Code will be uninterrupted, timely, secure or error-free
iii any errors in the Code obtained will be corrected.

WorldPay assumes no responsibility for errors or ommissions in the Code.

In no event shall WorldPay be liable to the Customer or any third parties for any special, punitive, incidental, indirect or consequential damages of any kind, or any damages whatsoever, including, without limitation, those resulting from loss of use, data or profits, whether or not WorldPay has been advised of the possibility of such damages, and on any theory of liability, arising out of or in connection with the use of the Code.

The use of the Code is at the Customer's own discretion and risk and with agreement that the Customer will be solely responsible for any damage to its computer system or loss of data that results from such activities. No advice or information, whether oral or written, obtained by the Customer from WorldPay shall create any warranty for the Code.
This code is provided on an "as is" basis and no warranty express or implied is provided. It is the responsibility of the customer to test its implementation and function.

Any use of the Code shall be deemed to be confirmation of the Customer's agreement to these supplemental terms.

------------------------------------------------------

1) If you are working on behalf of a client, please contact them and ask them to create a user account so that you can access the Installations page. If you do not have these details or you do not have a merchant yet please email support@worldpay.com asking for a test account.


2) To integrate you will need the Installation ID. To do so please login into the Merchant Administration Interface at http://www.worldpay.com/admin. Once you have logged in, go to the installations page. Please make a note of the installation ID.


3) If you want to use MD5 you will also need to set the MD5 secret Key in WorldPay. In the installations page, if you scroll down you will see a field that says "MD5 Secret". You will need to input the MD5 secret key. After, in the field below (Signature Fields) you would need to input the following

currency:amount:testMode:instId


4) Before copying the code into your web server make the following changes:

In worldpayconfig.php

Change the 'installationID'. If you have also set the MD5 secret please change it. It is the first line after the start of the Optional Parameters.

In paymentToken.php you can see the amount has been set to 10. This is the amount of the transaction. The developer should tie in your own software with this page so that the amount can be set dynamically.


5)After you have made these changes, upload the following files into your web server:

worldpayconfig.php
paymentToken.php

You can now process through test transactions.

Just browse to the paymentToken.php in your browser and test.

You can find a list of all the test cards that you can use:

http://www.worldpay.com/support/kb/bg/testandgolive/tgl5103.html

NOTE: To test successful transactions, the cardholder name should be "AUTHORISED", for refused transactions it should be "REFUSED", to simulate an error message it will need to be "ERROR". The expiry date is any date in the future, and any address details can be used. The CVC should be "555".

There are more features and functionality available in the worldpayconfig.php file.