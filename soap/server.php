<?php

function ItemQuery($itemname)
{
    srand(crc32($itemname));

    $result = Array('count' => rand(1,500), 'size' => rand(50,200));
    sleep(50);

    return $result;
}

$opts = Array();
$opts['compression'] = SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP;
$server = new SoapServer('test.wsdl', $opts);
$server->addFunction('ItemQuery');
$server->handle();