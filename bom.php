<?php
/*
$str = file_get_contents('C:/work/carglass-nl-kas/public/css/style.css');
$bom = pack("CCC", 0xef, 0xbb, 0xbf);
if (0 === strncmp($str, $bom, 3)) {
    echo "BOM detected - file is UTF-8\n";
    $str = substr($str, 3);
} else {
    echo "ELSE\n";
}
*/

// Removes BOM (Byte order mark) from file (if necessary)
function bomStrip( path, output )
{
    $bufsize = 65536;
    $utf8bom = "\\xef\\xbb\\xbf";

    $inf = fopen(path, r);
    $outf = fopen(output, w);

    $buf = fread($inf, strlen($utf8bom));
    if ($buf != $utf8bom)
    {
        fwrite($outf, $buf);
    }
    if ($buf == "")
    {
        exit();
    }
    while (true)
    {
        $buf = fread($inf, $bufsize);
        if ($buf == "")
        {
            exit();
        }
        fwrite($outf, $buf);
    }
}

?>