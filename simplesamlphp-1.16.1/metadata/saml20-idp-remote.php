<?php
/**
 * SAML 2.0 remote IdP metadata for SimpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-idp-remote 
 */


$metadata['https://sp.autoglass.com/shibboleth'] = array(
    //'SingleSignOnService'  => 'https://example.com/simplesaml/saml2/idp/SSOService.php',
    //'SingleLogoutService'  => 'https://example.com/simplesaml/saml2/idp/SingleLogoutService.php',
    'certificate'          => '../aviva.pem',
);