<?php

function getTestData() {
    $data = array(
        "postcode" => "ig11sb",
        "key" => "d4faa-f149e-3a655-01734",
        "response" => "data_formatted"
    );
    $data_string = json_encode($data);

    $ch = curl_init('http://pcls1.craftyclicks.co.uk/json/rapidaddress');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
    );

    $result = curl_exec($ch);
    return $result;
}

echo '<pre>'.getTestData().'</pre>';
?>