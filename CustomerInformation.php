<?php
require_once('CustomerInformationConfig.php');

class CustomerInformation
{
    private $mysqli;

    private $host;
    private $username;
    private $password;
    private $database;

    private $type;
    private $typeName;

    private $data;

    private $directory;
    private $incompleteBookingsDirectory;
    private $completeBookingsDirectory;

    private $filePath;

    private $filename;

    public function __construct($connection, $directories)
    {
        $this->directory = $directories['main'];
        $this->completeBookingsDirectory =  $directories['complete'];
        $this->incompleteBookingsDirectory =  $directories['incomplete'];

        $this->host = $connection['host'];
        $this->username = $connection['username'];
        $this->password = $connection['password'];
        $this->database = $connection['database'];
    }

    private function generateCSV($directory, $csv_array)
    {
        $date = new DateTime();
        $date->add(DateInterval::createFromDateString('yesterday'));

        ob_start();

        $this->filename = "uk-ideal-{$this->typeName}-bookings-" . $date->format('Y-m-d') . ".csv";
        $this->filePath = $directory . "/" . $this->filename;

        $output = fopen($this->filePath, 'w');
        foreach ($csv_array as $csv_line) {
            fputcsv($output, $csv_line);
        }
        fclose($output);

        return $this->filePath;
    }

    private function getResults($type, $limit)
    {
        $this->mysqli = new mysqli($this->host, $this->username, $this->password, $this->database);

        if ($this->mysqli->connect_errno) {
            die('Connect Error (' . $this->mysqli->connect_errno . ') '
                . $this->mysqli->connect_error);
        }

        $date = new \DateTime();
        $date->add(\DateInterval::createFromDateString('yesterday'));

        $query = "SELECT * FROM `ideal_uk_customer_information` WHERE `completed_booking` = {$type} AND date = '{$date->format('Y-m-d')}' ORDER BY `id` ASC";
        if (!empty($limit)) {
            $query .= " LIMIT 0, {$limit}";
        }

        $result = $this->mysqli->query($query);

        $count = mysqli_num_rows($result);

        $csv_array = array();
        if ($result) {
            // Cycle through results
            while ($row = $result->fetch_object()) {
                $csv_array[] = $row;
            }
        }

        return $csv_array;
    }

    private function generateCustomerInformationCSVArray($customers, $type)
    {
        $csv_array = array();

        if ($type === "incomplete") {
            $csv_array = array(
                'headers' =>
                    array(
                        'date',
                        'email'
                    )
            );

        } else if ($type === "complete") {
            $csv_array = array(
                'headers' =>
                    array(
                        'date',
                        'email',
                        'vrn',
                        'user_device'
                    )
            );
        }

        $additional_headers = array();

        $i = 0;
        foreach ($customers as $customer) {

            $i ++; // number of record

            if ($type === "incomplete") {
                $new_customer_data = array(
                    $customer->date,
                    $customer->email
                );
            } else if ($type === "complete") {
                $new_customer_data = array(
                    $customer->date,
                    $customer->email,
                    $customer->vrn,
                    $customer->user_device,
                );
            }

            $data = unserialize(stripslashes($customer->extra_data));

            if ($data) {
                // check for additional data headers
                foreach ($data as $key => $value) {
                    if (!in_array($key, $csv_array['headers'])) {
                        if ($key != "job_id" && $key != "job_address") {
                            $csv_array['headers'][] = $key;
                            $additional_headers[] = $key;
                        }
                    }
                }

                foreach ($additional_headers as $additional_header) {
                    if ($additional_header != "job_address") {
                        if (array_key_exists($additional_header, $data)) {
                            $new_customer_data[] = $data[$additional_header];
                        } else {
                            $new_customer_data[] = '';
                        }
                    }
                }
            }

            $csv_array[] = $new_customer_data;
        }

        return $csv_array;
    }

    public function processCustomerInformation($type = null, $limit = null)
    {
        $this->typeName = $type;
        $this->type = null;
        if ($type === "incomplete") {
            $this->type = 0;
        } else if ($type === "complete") {
            $this->type = 1;
        } else {
            die ("Parameter for booking status isn't defined.");
        }

        $csv_array = $this->getResults($this->type, $limit);

        $this->data = $this->generateCustomerInformationCSVArray($csv_array, $type);

        $bookingDirectory = null;

        if ($this->typeName === "incomplete") {
            $bookingDirectory = $this->directory . "/" . $this->incompleteBookingsDirectory . "/";
        } else if ($this->typeName === "complete") {
            $bookingDirectory = $this->directory . "/" . $this->completeBookingsDirectory . "/";
        }

        $this->generateCSV($bookingDirectory, $this->data);

        $this->mysqli->close();
    }
}

date_default_timezone_set('UTC');

//$connection and $directories are defined in CustomerInformationConfig.php so that it is easier to modify and maintain the two scripts on windows local and server environments
$customerInformation = new CustomerInformation($connection, $directories);
$customerInformation->processCustomerInformation("complete");
$customerInformation->processCustomerInformation("incomplete");

unset($customerInformation);