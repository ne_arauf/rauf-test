<?php
session_start();
$worldpayLib = str_replace("\\", "/", dirname(__FILE__)) . "/worldpay-lib-php-1.6/lib/worldpay.php";

include_once($worldpayLib);

$worldpay = new Worldpay('T_S_43a4203f-862f-49bc-9c1b-b728aa112dd0');

$billing_address = array(
    "address1" => '123 House Road',
    "address2" => 'A village',
    "address3" => '',
    "postalCode" => 'EC1 1AA',
    "city" => 'London',
    "state" => '',
    "countryCode" => 'GB',
);
var_dump($_POST);

$response = null;
try {
    $response = $worldpay->createOrder(array(
        'token' => $_POST['token'],
        'amount' => 5000,
        'currencyCode' => 'GBP',
        'name' => $_POST['name'],
        'billingAddress' => $billing_address,
        'orderDescription' => 'Order description',
        'customerOrderCode' => 'Order code',
        'is3DSOrder' => true,
    ));
    $_SESSION['orderCode'] = $response['orderCode'];
    $oneTime3DsToken = $response['oneTime3DsToken'];
    $redirectURL = $response['redirectURL'];
} catch (WorldpayException $e) {
    echo 'Error code: ' . $e->getCustomCode() . '
  
    HTTP status code:' . $e->getHttpStatusCode() . '
  
    Error description: ' . $e->getDescription() . '
 
    Error message: ' . $e->getMessage();
}

var_dump($response);
?>
<html>
    <head>
        <title>3-D Secure helper page</title>
    </head>
    <body OnLoad="OnLoadEvent();">
        This page should forward you to your own card issuer for 
        identification. If your browser does not start loading the 
        page, press the Submit button. <br/>
        After you successfully identify yourself you will be sent back 
        to this website, where the payment process will continue.<br/>
        <form name="theForm" method="POST" action="<?php echo $redirectURL; ?>"><br />
            PaReq: <input type="text" name="PaReq" value="<?php echo $oneTime3DsToken; ?>" /><br />
            TermUrl: <input type="text" name="TermUrl" value="http://test.localhost/payment/verified.php" /><br />
            MD: <input type="text" name="MD" value="<?php echo $response['orderCode']; ?>" />
            <input type="submit" name="Identify yourself" />
        </form>
        <script language="Javascript">
            <!--
                function OnLoadEvent()
            {
                // Make the form post as soon as it has been loaded.
                //document.theForm.submit();
            }
            // -->
        </script>
    </body>
</html>
