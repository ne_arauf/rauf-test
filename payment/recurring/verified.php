<?php
session_start();
$worldpayLib = str_replace("\\", "/", dirname(__FILE__)). "/../worldpay-lib-php-1.6/lib/worldpay.php";

include_once($worldpayLib);

$worldpay = new Worldpay('T_S_43a4203f-862f-49bc-9c1b-b728aa112dd0');

$response = null;
$_POST['PaRes'] = "IDENTIFIED";
var_dump("POST", $_POST);
var_dump("SESSION", $_SESSION['orderCode']);
try {
    $response = $worldpay->authorise3DSOrder($_SESSION['orderCode'], $_POST['PaRes']);
    if (isset($response['paymentStatus']) && $response['paymentStatus'] == 'SUCCESS') {
        echo 'Order Code: ' . $_SESSION['orderCode'] . ' has been authorised <br/>';
    } else {
        echo 'There was a problem authorising 3DS order <br/>';
    }
} catch (WorldpayException $e) {
    echo 'Error code: ' .$e->getCustomCode() .'
  
    HTTP status code:' . $e->getHttpStatusCode() . '
  
    Error description: ' . $e->getDescription()  . '
 
    Error message: ' . $e->getMessage();
}

$details = $worldpay->getStoredCardDetails($response['token']);

var_dump("DETAILS", $details);

$worldpay->deleteToken($response['token']);

$details = $worldpay->getStoredCardDetails($response['token']); // to check if token still exists

var_dump("DETAILS", $details);
?>
<html>
    <head>
        <title>3-D Secure helper page</title>
    </head>
    <body>
        <?php var_dump($response); ?>
    </body>
</html>

<?php session_destroy(); ?>
