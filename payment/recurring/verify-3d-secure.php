<?php
session_start();
$worldpayLib = str_replace("\\", "/", dirname(__FILE__)) . "/../worldpay-lib-php-1.6/lib/worldpay.php";

echo $worldpayLib . "<br />";

include_once($worldpayLib);

$worldpay = new Worldpay('T_S_43a4203f-862f-49bc-9c1b-b728aa112dd0');

$billing_address = array(
    "address1" => '123 House Road',
    "address2" => 'A village',
    "address3" => '',
    "postalCode" => 'EC1 1AA',
    "city" => 'London',
    "state" => '',
    "countryCode" => 'GB',
);
var_dump($_POST);

$responseStandard = null;
$responseRecurring = null;
try {
    $responseStandard = $worldpay->createOrder(array(
        'token' => $_POST['token'],
        'amount' => 5000,
        'currencyCode' => 'GBP',
        'name' => $_POST['name'],
        'billingAddress' => $billing_address,
        'orderDescription' => 'Order description',
        'customerOrderCode' => 'Order code',
        'is3DSOrder' => true,
    ));
    $_SESSION['orderCode'] = $responseStandard['orderCode'];
    $oneTime3DsToken = $responseStandard['oneTime3DsToken'];
    $redirectURL = $responseStandard['redirectURL'];
} catch (WorldpayException $e) {
    echo 'Error code: ' . $e->getCustomCode() . '
  
    HTTP status code:' . $e->getHttpStatusCode() . '
  
    Error description: ' . $e->getDescription() . '
 
    Error message: ' . $e->getMessage();
}

var_dump($responseStandard);
var_dump($responseStandard['paymentResponse']['cardType']);

if ($responseStandard['paymentResponse']['cardType'] === "VISA_CREDIT") {
    $surcharge = 2000;
    try {
        $responseRecurring = $worldpay->createOrder(array(
            'token' => $_POST['token'],
            'amount' => $responseStandard['amount'] + $surcharge,
            'currencyCode' => 'GBP',
            'name' => $_POST['name'],
            'billingAddress' => $billing_address,
            'orderDescription' => 'Order description',
            'customerOrderCode' => 'Order code',
            'is3DSOrder' => true,
        ));
        $_SESSION['orderCode'] = $responseRecurring['orderCode'];
        $oneTime3DsToken = $responseRecurring['oneTime3DsToken'];
        $redirectURL = $responseRecurring['redirectURL'];
    } catch (WorldpayException $e) {
        echo 'Error code: ' . $e->getCustomCode() . '
  
    HTTP status code:' . $e->getHttpStatusCode() . '
  
    Error description: ' . $e->getDescription() . '
 
    Error message: ' . $e->getMessage();
    }
}

var_dump($responseRecurring);
?>
<html>
    <head>
        <title>3-D Secure helper page</title>
    </head>
    <body OnLoad="OnLoadEvent();">
        This page should forward you to your own card issuer for 
        identification. If your browser does not start loading the 
        page, press the Submit button. <br/>
        After you successfully identify yourself you will be sent back 
        to this website, where the payment process will continue.<br/>
        <form name="theForm" method="POST" action="<?php echo $redirectURL; ?>"><br />
            PaReq: <input type="text" name="PaReq" value="<?php echo $oneTime3DsToken; ?>" /><br />
            TermUrl: <input type="text" name="TermUrl" value="http://test.localhost/payment/recurring/verified.php" /><br />
            MD: <input type="text" name="MD" value="<?php echo $responseRecurring['orderCode']; ?>" /><br />
            Token: <input type="text" name="token" value="<?php echo $responseRecurring['token']; ?>" /><br />
            <input type="submit" name="Identify yourself" />
        </form>
        <script language="Javascript">
            <!--
                function OnLoadEvent()
            {
                // Make the form post as soon as it has been loaded.
                //document.theForm.submit();
            }
            // -->
        </script>
    </body>
</html>
