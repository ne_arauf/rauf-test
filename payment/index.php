<!DOCTYPE html> 
<html>   
    <head>     
        <title></title>
        <meta charset="UTF-8" />
        <script src= "https://cdn.worldpay.com/v1/worldpay.js">
        </script>        

        <script type='text/javascript'>
            window.onload = function () {
                Worldpay.setClientKey('T_C_836bfe5e-f5e7-4130-a737-862b594d2ed3');
                Worldpay.reusable = false;
                Worldpay.templateSaveButton = false;
                Worldpay.useTemplate('demoForm', 'paymentDetailsHere', 'inline', function (obj) {
                    var _el = document.createElement('input');
                    _el.value = obj.token;
                    _el.type = 'hidden';
                    _el.name = 'token';
                    document.getElementById('demoForm').appendChild(_el);
                });
            }
        </script>
    </head>   

    <body>     
        <form action="verify-3d-secure.php" id="demoForm" method="post">     
            <div id='paymentDetailsHere'></div>
            <div>
                <input type='text' name='name' id='name' placeholder="Test type here..." />
                <input type='button' onclick='Worldpay.submitTemplateForm()' value='Place Order' />
            </div>
        </form>
    </body> 
</html> 