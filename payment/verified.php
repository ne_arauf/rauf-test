<?php
session_start();
$worldpayLib = str_replace("\\", "/", dirname(__FILE__)). "/worldpay-lib-php-1.6/lib/worldpay.php";

include_once($worldpayLib);

$worldpay = new Worldpay('T_S_43a4203f-862f-49bc-9c1b-b728aa112dd0');

$response = null;
$_POST['PaRes'] = "NOT_IDENTIFIED";
var_dump($_POST['PaRes']);
try {
    $response = $worldpay->authorise3DSOrder($_SESSION['orderCode'], $_POST['PaRes']);
    if (isset($response['paymentStatus']) && $response['paymentStatus'] == 'SUCCESS') {
        echo 'Order Code: ' . $_SESSION['orderCode'] . ' has been authorised <br/>';
    } else {
        echo 'There was a problem authorising 3DS order <br/>';
    }
} catch (WorldpayException $e) {
    echo 'Error code: ' .$e->getCustomCode() .'
  
    HTTP status code:' . $e->getHttpStatusCode() . '
  
    Error description: ' . $e->getDescription()  . '
 
    Error message: ' . $e->getMessage();
}
?>
<html>
    <head>
        <title>3-D Secure helper page</title>
    </head>
    <body>
        <?php var_dump($response); ?>
    </body>
</html>

<?php session_destroy(); ?>
