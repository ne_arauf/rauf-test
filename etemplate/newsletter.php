<!DOCTYPE html>
<html>
<head>
    <title>Autoglass© booking ref: 0000012345</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <style type="text/css">
        /* CLIENT-SPECIFIC STYLES */
        body, table, td, a {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        /* Prevent WebKit and Windows mobile changing default text sizes */
        table, td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        /* Remove spacing between tables in Outlook 2007 and up */
        img {
            -ms-interpolation-mode: bicubic;
        }

        /* Allow smoother rendering of resized image in Internet Explorer */

        /* RESET STYLES */
        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }

        table {
            border-collapse: collapse !important;
        }

        body {
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            width: 100% !important;
        }

        /* iOS BLUE LINKS */
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }
        h1, h2, h3, h4, h5, h6, h7 {
            font-family: arial, sans-serif !important;
            margin: 15px;
        }
        p {
            color: inherit !important;
            text-decoration: none !important;
            font-size: 12px !important;
            font-family: arial, sans-serif !important;
            font-weight: inherit !important;
            line-height: 15px !important;
            margin: 15px;
        }

        /* MOBILE STYLES */
        @media screen and (max-width: 525px) {

            /* ALLOWS FOR FLUID TABLES */
            .wrapper {
                width: 100% !important;
                max-width: 100% !important;
            }

            /* ADJUSTS LAYOUT OF LOGO IMAGE */
            .logo img {
                margin: 0 auto !important;
            }

            /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
            .mobile-hide {
                display: none !important;
            }

            .img-max {
                max-width: 100% !important;
                width: 100% !important;
                height: auto !important;
            }

            /* FULL-WIDTH TABLES */
            .responsive-table {
                width: 100% !important;
            }

            /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
            .padding {
                padding: 10px 5% 15px 5% !important;
            }

            .padding-meta {
                padding: 30px 5% 0px 5% !important;
                text-align: center;
            }

            .no-padding {
                padding: 0 !important;
            }

            .section-padding {
                padding: 50px 15px 50px 15px !important;
            }

            /* ADJUST BUTTONS ON MOBILE */
            .mobile-button-container {
                margin: 0 auto;
                width: 100% !important;
            }

            .mobile-button {
                padding: 15px !important;
                border: 0 !important;
                font-size: 16px !important;
                display: block !important;
            }

        }

        /* ANDROID CENTER FIX */
        div[style*="margin: 16px 0;"] {
            margin: 0 !important;
        }
    </style>
</head>
<body style="margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    &nbsp;
</div>

<!-- HEADER -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#FFF" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="1000">
                <tr>
                    <td align="center" valign="top" width="1000">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 1000px;" class="wrapper">
                <tr>
                    <td align="center" valign="top" style="padding: 15px 0;" class="logo">
                        <a href="http://www.autoglass.co.uk/" target="_blank">
                            <img alt="Logo" src="logo.gif" width="220" height="50"
                                 style="display: block; font-family: Arial, sans-serif; color: #ffffff; font-size: 16px;"
                                 border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFF" align="center" style="padding: 70px 15px 70px 15px;" class="section-padding">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="1000">
                <tr>
                    <td align="center" valign="top" width="1000">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 1000px;"
                   class="responsive-table">
                <tr>
                    <td align="center" height="100%" valign="top" width="100%">
                        <!--[if (gte mso 9)|(IE)]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="1000">
                            <tr>
                                <td align="center" valign="top" width="1000">
                        <![endif]-->
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
                               style="max-width:1000px;">
                            <tr>
                                <td align="center" valign="top" style="font-size:0;" class="padding">
                                    <!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="1000">
                                        <tr>
                                            <td align="left" valign="top" width="240">
                                    <![endif]-->
                                    <div style="display:inline-block; margin: 0 -2px; max-width:70%; min-width:240px; vertical-align:top; width:100%;"
                                         class="wrapper">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%"
                                               style="max-width:700px;" class="wrapper">
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td style="padding: 20px 0 30px 0;">
                                                                <table cellpadding="0" cellspacing="0" border="0"
                                                                       width="100%">
                                                                    <tr>
                                                                        <td align="left"
                                                                            style="padding: 15px 0 0 0; font-family: Arial, sans-serif; color: #000; font-size: 20px;"
                                                                            bgcolor="#FFF">Hi Stacy,
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left"
                                                                            style="padding: 5px 0 0 0; font-family: Arial, sans-serif; color: #000; font-size: 14px; line-height: 20px;"
                                                                            bgcolor="#FFF">We're going to replace your
                                                                            windscreen:
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 20px 0 30px 0;">
                                                                <table cellpadding="0" cellspacing="0" border="0"
                                                                       width="100%">
                                                                    <tr style="border-bottom: 1px dotted black !important;">
                                                                        <td align="center"
                                                                            style="padding: 15px 0 0 0; font-family: Arial, sans-serif; color: #000; font-size: 20px; line-height: 50px;"
                                                                            bgcolor="#FFF">TUESDAY APRIL 18TH,
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="border-bottom: 1px dotted black !important;">
                                                                        <td align="center"
                                                                            style="padding: 5px 0 0 0; font-family: Arial, sans-serif; color: #000; font-size: 20px; line-height: 50px;"
                                                                            bgcolor="#FFF">2-4 PM
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="border-bottom: 1px dotted black !important;">
                                                                        <td align="center"
                                                                            style="padding: 5px 0 0 0; font-family: Arial, sans-serif; color: #000; font-size: 20px; line-height: 50px;"
                                                                            bgcolor="#FFF">MK44 3US
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    <td width="20" style="font-size: 1px;">&nbsp;</td>
                                    <td align="right" valign="top" width="240">
                                    <![endif]-->
                                    <div style="display:inline-block; margin: 0 -2px; max-width:30%; min-width:240px; vertical-align:top; width:100%;"
                                         class="wrapper">
                                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                            <tr>
                                                <td>
                                                    <table align="left" border="0" cellpadding="0" cellspacing="0"
                                                           width="100%" style="max-width:240px; float: right;"
                                                           class="wrapper">
                                                        <tr>
                                                            <td align="center" valign="top">

                                                                <table border="0" cellpadding="0" cellspacing="0"
                                                                       width="100%">
                                                                    <tr>
                                                                        <td style="padding: 20px 0 30px 0;"
                                                                            bgcolor="#028B00">
                                                                            <table cellpadding="0" cellspacing="0"
                                                                                   border="0" width="100%"
                                                                                   bgcolor="#028B00">
                                                                                <tr>
                                                                                    <td align="center"
                                                                                        style="padding: 15px 0 0 0; font-family: Arial, sans-serif; color: #FFF; font-size: 20px;"
                                                                                        bgcolor="#028B00">Manage Your
                                                                                        Appointment
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center"
                                                                                        style="padding: 5px 0 0 0; font-family: Arial, sans-serif; color: #666666; font-size: 14px; line-height: 20px;"
                                                                                        bgcolor="#028B00"><a
                                                                                                style="min-width: 200px; height: 100px; background-color: #FFF"
                                                                                                href="#">SIGN IN ></a>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </table>
                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFF" align="center">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="1000">
                <tr>
                    <td align="center" valign="top" width="1000">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 1000px;" class="wrapper">
                <tr>
                    <td align="left" valign="top" style="padding: 15px 0;" class="logo">
                        <h3 style="font-size: 14px; color: #666;">Our Terms of Business</h3>

                        <p>
                            <strong>By proceeding with your appointment, you agree that you have read and fully
                                understood our Terms of Business. </strong></p>
                        <p>
                            If your vehicle has an ADAS camera that includes Adaptive Cruise Control (ACC) or Automated
                            Emergency Braking (AEB) or other functionality where the vehicle takes corrective action
                            while you are driving and we have not discussed that with you at the time of our booking,
                            please contact us so that we can check whether we think your camera is of a type that may
                            need recalibration following a windscreen replacement. You can also check your vehicle’s
                            owner manual for information on whether recalibration is required for your vehicle ADAS
                            system.
                        </p>
                        <p>
                            Our service to you will be governed by the following terms and conditions:
                        </p>
                        <p>
                            1. You are dealing with Belron UK Limited, trading as Autoglass®. In these terms of
                            business, references to “we” or “Autoglass®” or “our” means Belron UK Limited. By asking
                            Autoglass® to carry out vehicle glass repair and replacement work on your vehicle you agree
                            that our work shall be governed by the following terms. Our contract with you is formed at
                            the time of you making your appointment with us. Most but not all of the terms set out below
                            are reproduced on the sales document that you (or your representative) will be asked to sign
                            when we attend your vehicle. If our work is the subject of a claim on an insurance policy
                            and you (the person making the booking with us) are not the policyholder for the vehicle
                            that is the subject of the claim, you promise that your dealings with us are made with the
                            authority of the policyholder. If we have not been appointed as agent of the insurer to
                            handle a claim under a policy issued by that insurer and our work includes assisting you in
                            a making a claim under a policy issued by that insurer, you appoint us as your agent to
                            assist in the administration and performance of your insurance policy. The signature (made
                            by you or your representative) that we receive at the time of the appointment confirms our
                            appointment as agent of the policyholder on that basis.
                        </p>
                        <p>
                            2. Where relevant and available to us, we check the latest information submitted to us by
                            insurers to confirm that our work may be paid for under your insurance policy. Where
                            practicable, Autoglass® will contact your insurer directly to verify the nature and scope of
                            your insurance cover. You acknowledge that Autoglass® is not responsible for the extent of
                            your policy cover or its conditions (such as excess payable).
                        </p>
                        <p>
                            3. In the course of repair, glass may crack beyond repair through no fault of our
                            technician. You acknowledge that risk. If that happens, we will ask if you would like us to
                            replace the glass. If you wish to proceed with replacement, you agree that paragraph 4 below
                            shall apply to that service. We will take into account any excess you may have already paid
                            to us.
                        </p>
                        <p>
                            4. Payments up front and in part. You agree to be responsible for the full cost of our glass
                            repair and replacement work on your vehicle. In making arrangements to deliver our service
                            to you, we incur business costs. You agree that we can immediately charge (i) for or in
                            respect of these arrangements or costs and/or (ii) an up front part payment in respect of
                            our glass service.
                            If you are making an insurance claim in respect of our service, we are entitled, immediately
                            upon any cost or charge as aforesaid being incurred or made, (i) to obtain any excess, or
                            other portion of the insurance claim which your insurance policy obliges you to pay or fund
                            for or from your own account, from you and (ii) to apply such excess or portion against any
                            such cost or charge.
                            Assignment: If you are making an insurance claim in respect of our service, you agree that,
                            with immediate effect from the time we begin work on your vehicle, (i) you have assigned to
                            us your right to collect the claim proceeds, and (ii) you shall take any steps necessary or
                            reasonably beneficial, including signing any further document, to prove or otherwise give
                            all practical or legal effect to such assignment.
                        </p>
                        <p>
                            5. If the policyholder is registered for VAT, the policyholder agrees to pay VAT on our
                            service. When you booked your appointment, you may have paid VAT calculated on the cost of
                            the materials we usually use for a vehicle like yours. If our work on your vehicle requires
                            us to use materials (such as a trim or clip) that we did not expect to use for the job, the
                            price we charge and the VAT due from you may change. In that case, you agree to pay all
                            additional costs and VAT due to us and we agree to refund our costs and VAT to the extent
                            the amount you should have paid reduces.
                        </p>
                        <p>
                            6. The visibility of a repair to glass depends on the nature of the damage at the time of
                            repair. If you are not happy with the look of our repair, we can replace your windscreen if
                            you pay the required amount.
                        </p>
                        <p>
                            7. A diagrammatic record of visible damage existing on your vehicle will be made by our
                            technician before starting work on your vehicle. We exclude all liability for repair of
                            damage, whether visible or not, existing before we began to work on your vehicle.
                        </p>
                        <p>
                            8. We will perform our service to the best of our ability at a time and location to be
                            agreed between us. We reserve the right to change our appointment time with you for any
                            reason. We will notify you in advance of any change to the agreed appointment time. If you
                            have a complaint about our service, please call 0800 011 3896 or write to Customer Services
                            c/o Autoglass® at the address shown at the bottom of this page or email us using the
                            following link customer.services@autoglass.co.uk. If we damage your vehicle, we can arrange
                            its repair at no cost to you. If without our prior written approval you organise a repair
                            yourself, we do not guarantee to pay the costs you incur.
                        </p>
                        <p>
                            9. Key limitations to our work: (1) We are unable to install a replacement windscreen on
                            corroded metal. If we find your vehicle to be corroded, we will stop work and explain your
                            options; and (2) Certain vehicles now have Advanced Driver Assistance Systems (ADAS) cameras
                            mounted on their windscreen. Vehicle manufacturers specify that certain of those ADAS
                            cameras require recalibration following replacement of the vehicle’s windscreen. Autoglass®
                            is not able to provide that service for some vehicle types. Where we are not able to effect
                            calibration following completion of a windscreen replacement, you may arrange for your
                            vehicle’s camera to be recalibrated at your own cost by an alternative supplier, which may
                            include the vehicle’s dealership network. Where recalibration is recommended for your
                            vehicle’s ADAS camera, before making your booking with us, we will advise you of that fact
                            and ask whether you would like us to calibrate your ADAS camera. Where we do not offer
                            calibration for your vehicle’s ADAS camera, we will check that you wish us to proceed with
                            replacement of your vehicle’s windscreen.
                        </p>
                        <p>
                            10. Our liability to you: You acknowledge that the cost to us of repairing your vehicle in
                            the event that we damage it is likely to exceed the amount we are paid for our service. You
                            agree that our total liability to you both for any service failure or vehicle damage is
                            limited to: (A) the total cost of repairing any damage we cause to your vehicle; plus (B)
                            for any period where your own car will be unavailable to you, at our choice, the cost to us
                            of providing you with, or paying for, alternative means of transport or a replacement
                            vehicle of our choice (acting reasonably). Except as stated above, you agree that unless we
                            have written to you to confirm otherwise before we begin work, you and Autoglass® do not
                            intend that Autoglass® reimburse or compensate you for loss of income, loss of use of your
                            vehicle, costs or expenses incurred from loss of use of your vehicle, loss of business or
                            profits or pure economic loss or indirect or consequential loss suffered by you as a result
                            of our work.
                        </p>
                        <p>
                            11. Dealerships &amp; accident management companies: If you allow a dealership or an
                            accident management company to manage the repair process in relation to your vehicle, you
                            are likely to incur costs in excess of those we would incur if you allow us to manage the
                            repair on your behalf. A dealership or accident management company is likely to offer to
                            provide you with a replacement vehicle. The costs of that vehicle is likely to be very high
                            (thousands of pounds) – much higher than the cost to us of organising a rental on your
                            behalf. We will not pay for the costs of any vehicle supplied to you unless we have arranged
                            the rental directly on your behalf. Please be aware the costs to the dealership and accident
                            management company will be your personal liability and you may be at significant financial
                            risk if you choose to use their services.
                        </p>
                        <p>
                            12. Nothing shall limit our liability for fraud or death or personal injury caused by our
                            negligence. Your statutory rights are not affected.
                        </p>
                        <p>
                            13. Where payment for our work on your vehicle will be made using a trade or company
                            account, our trade/company terms will apply to our work in priority to the terms written
                            above.
                        </p>
                        <p>
                            14. You authorise us to remove your damaged vehicle glass as waste (European Waste Catalogue
                            16-01-20) to an authorised site for appropriate treatment (recycling).
                        </p>
                        <p>
                            15. Autoglass® is authorised and regulated by the Financial Conduct Authority in respect of
                            insurance mediation services only. Our authorisation number is 314575.
                        </p>
                        <p>
                            16. You agree that our <a
                                    href="https://www.autoglass.co.uk/privacy-policy/?ins=wt16&utm_source=confirmation-replacement&utm_medium=email&utm_content=confirmation-replacement-remedy-2016-11&utm_campaign=transactional">privacy
                                policy</a> and <a
                                    href="https://www.autoglass.co.uk/cookie-use-policy/?ins=wt16&utm_source=confirmation-replacement&utm_medium=email&utm_content=confirmation-replacement-remedy-2016-11&utm_campaign=transactional">policy
                                on cookies</a> as it or they appear from time to time on our website shall govern the
                            handling of your personal information that we receive from you, your insurer or other third
                            party (such as a broker). If you wish to limit our right to use your personal information,
                            please write to Data Protection c/o Autoglass® at the address shown below.
                        </p>
                        <p>
                            17. At Autoglass®, we are committed to reducing fraud made using credit cards. We reserve
                            our right not to accept payment from you by debit or credit card where we suspect that by
                            doing so a fraud may be perpetrated against us or the registered card holder.
                        </p>
                        <p>
                            18. Our terms of business and any dispute or claim arising out of or in connection with them
                            or their subject matter (including non-contractual disputes or claims of any kind arising
                            directly or indirectly) shall be governed by and construed in accordance with the laws of
                            England and Wales.
                        </p>
                        <p>
                            19. The courts of England will have exclusive jurisdiction over any claim arising from, or
                            related to, our goods and services although we retain the right to bring proceedings against
                            you for breach of these conditions in your country of residence or any other relevant
                            country.
                        </p>
                        <p>
                            Consumer rights information
                        </p>
                        <p>
                            20. Any booking that you make with us by phone or online will be subject to The Consumer
                            Contracts (Information, Cancellation and Additional Charges) Regulations 2013 (SI 2013/3134)
                            (as amended from time-to-time). We are pleased to provide you with the following information
                            in accordance with those regulations:
                        </p>
                        <p>
                            a. Autoglass® is the trading name of Belron UK Limited, company number 494648 whose
                            registered office and head office location is at 1 Priory Business Park, Cardington,
                            Bedford, Bedfordshire MK44 3US. Telephone number 0800 011 3896. Fax number: 01234 831 100.
                            You can contact us using the following link: customer.services@Autoglass.co.uk
                        </p>
                        <p>
                            b. Autoglass® supplies vehicle glass repair and replacement services and products.
                        </p>
                        <p>
                            c. If you are insured, normally you only need to pay us an excess, if applicable. If you
                            wish to pay in full for our work yourself, we will provide you with a quote for our work
                            including VAT. If you intend that your insurer pay us the balance above the excess for our
                            work and you are VAT registered, we are instructed by your insurer to ask you to pay the
                            applicable VAT on our charges. When we accept your booking and, where required, quote the
                            price of our service, we make assumptions as to what parts your vehicle will require. If
                            those assumptions prove to be incorrect, our costs of service and therefore our price will
                            change to take account of the parts we require for our work. For example, we may have to
                            replace a vehicle glass trim that usually we do not have to replace for a vehicle like
                            yours. We reserve our right to adjust our price and, where applicable, the amount of VAT we
                            charge you according to the goods we supply to you in the course of providing our service to
                            you.
                        </p>
                        <p>
                            d. Where you make a claim under your insurance and we have received the relevant information
                            from your insurer, we will advise you of the excess we believe you are required to pay and
                            will claim the balance of our fee from your insurer.
                        </p>
                        <p>
                            e. Where we have been unable to validate your policy details in advance of providing our
                            services to you, we shall rely on the information that you provided to us. We reserve all
                            our rights to recover payment for our services in all circumstances including without
                            limitation where information you have provided to us was incorrect regardless of whether you
                            are at fault.
                        </p>
                        <p>
                            f. We make no additional charge for delivery of the products that we will use in the course
                            of providing our service. We accept payment using Visa, MasterCard, Visa Electron or
                            Maestro. We do not accept cash, American Express, Diners Club, PayPal or personal cheques.
                            Payment by business/company cheque is available for trade account holders only.
                        </p>
                        <p>
                            g. We will provide our service to you by appointment at a location agreed between us.
                        </p>
                        <p>
                            h. Our contract with you is for the supply of vehicle glass repair and replacement services.
                            Normally, under the Regulations, if you are a consumer, you will have a right to cancel our
                            contract for 14 working days counting from the day after our contract with you was formed.
                            When we are given the keys to your vehicle, we shall understand that to be your express
                            consent to begin our work on your vehicle. Usually, that means we will have begun our work
                            before the end of the usual cancellation period. Your cancellation rights under the
                            Regulations will end when that work starts. You may cancel your appointment and receive a
                            full refund of any monies that you have paid to us at any time before we commence repair or
                            replacement work on your vehicle. To cancel our service, please call us, contact us via our
                            website or tell our technician before he starts work on your vehicle. We do not require you
                            to complete any special form to cancel your appointment. We recommend that the easiest way
                            to cancel your appointment is to call us on 0800 36 36 36 which is free from a landline. You
                            may prefer to print off this page and complete the following details and post the section
                            below to us if that is easier for you:
                            <br/>
                            <br/>
                            <br/>
                        <p>
                            To: Autoglass® (Belron UK Limited), 1 Priory Business Park Cardington, Bedford, Bedfordshire
                            MK44 3US
                            <br/>
                            <br/>
                            <br/>
                            I: _____________________________________________________________________
                            <br/>
                            <br/>
                            give notice that I request to cancel my order for the following service:
                            <br/>
                            <br/>
                            <br/>
                            ________________________________________________________________________
                            <br/>
                            <br/>
                            <br/>
                            Vehicle registration number: ___________________________________________
                            <br/>
                            <br/>
                            <br/>
                            Appointment date: ___________________________________________________
                            <br/>
                            <br/>
                            <br/>
                            Your address: _______________________________________________________
                            <br/>
                            <br/>
                            <br/>
                            _______________________________________________________
                            <br/>
                            <br/>
                            <br/>
                            _______________________________________________________
                            <br/>
                            <br/>
                            <br/>
                            _______________________________________________________
                            <br/>
                            <br/>
                            <br/>
                            Your telephone number (in case we need to call you): _______________________
                            <br/>
                            <br/>
                            Your signature:
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            Date: _______________________
                            <br/>
                            <br/>
                            <br/>
                        </p>
                        <p>
                            Please note that you may not cancel your appointment after we have commenced our work on
                            your vehicle and we reserve all rights to recover payment for our service in circumstances
                            where you attempt to cancel your appointment or require us to cease work on your vehicle
                            after our work has commenced.
                        </p>

                        <p>
                            i. The validity of any price we offer you for our services is limited to the duration of the
                            call during which the offer was made. Accordingly, we may refuse to honour a price offered
                            to you where you do not agree to purchase the relevant service during the continuation of
                            the call in which the offer was first communicated to you.
                        </p>
                        <p>
                            j. Where we have taken any payment from you in the course of you making an appointment with
                            us, we shall hold that payment to your order until the moment we begin our work on your
                            vehicle. With effect from commencement of our work on your vehicle, we reserve our right to
                            retain for our account any payment you have made to us.
                        </p>
                        <p>
                            k. The only language offered by us for any contract is English.
                        </p>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
</table>
</body>
</html>
