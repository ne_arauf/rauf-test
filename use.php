<?php

$message = 'hello';

// No "use"
$example = function () {
    echo "No 'use': ";
    var_dump($message);
    echo "<br />";
};
echo $example() . "<br />";

// Inherit $message
$example = function () use ($message) {
    echo "Inherit $ message: ";
    var_dump($message);
    echo "<br />";
};
echo $example() . "<br />";

// Inherited variable's value is from when the function
// is defined, not when called
$message = 'world';
echo "defined: ";
echo $example() . "<br />";

// Reset message
$message = 'hello';

// Inherit by-reference
$example = function () use (&$message) {
    echo "Inherit by reference: ";
    var_dump($message);
    echo "<br />";
};
echo $example() . "<br />";

// The changed value in the parent scope
// is reflected inside the function call

$message = 'world';
echo "Inside the function call: ";
echo $example() . "<br />";

// Closures can also accept regular arguments
$example = function ($arg) use ($message) {
    echo "Regular arguments: ";
    var_dump($arg . ' ' . $message);
    echo "<br />";
};
$example("hello") . "<br />";

?>